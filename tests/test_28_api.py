#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_api_28 import PaymentApi28
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test28(BaseClass):

    def test_standard_payment(self, getData):
        # Transakce založená přes legacy API
        tc28 = PaymentApi28(self.driver)
        # logger
        log = self.getLogger()
        tc28.select_test().click()
        tc28.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc28.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc28.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc28.fill_expiration().send_keys(getData["expiration"])
        tc28.fill_cvc().send_keys(getData["cvc"])
        try:
            tc28.save_card_check().click()
            not_found = False
        except:
            log.warning("Platba bez moznosti ulozit kartu - podle scenare OK")
            not_found = True
        assert not_found
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc28.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()=" legacy API, primary code: 0, secondary code: 0"]')
        log.info("Test case Transakce založená přes legacy API OK")
        tc28.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_02_standard_payment)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param