#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err602_23 import PaymentErr23
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test23(BaseClass):

    def test_standard_payment(self, getData):
        # Platba pomocí pt@shop (ČSOB)
        tc23 = PaymentErr23(self.driver)
        # logger
        log = self.getLogger()
        tc23.select_test().click()
        tc23.confirm_test().click()
        try:
            assert tc23.error_block().is_displayed()
            log.info("Error message 602 displayed - OK")
            state = True
        except:
            log.warning("No error message - NOK")
            state = False
        assert state
        tc23.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param