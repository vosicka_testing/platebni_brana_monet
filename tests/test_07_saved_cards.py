#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_saved_cards_07 import PaymentSavedCards07
from TestData.Brana_test_data import HomePageData
from selenium.webdriver import ActionChains

import time
import pytest


class Test07(BaseClass):

    def test_standard_payment(self, getData):
        # objekt pro kliknutí na specifický objekt
        actionChains = ActionChains(self.driver)
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc07 = PaymentSavedCards07(self.driver)
        # logger
        log = self.getLogger()
        tc07.select_test().click()
        tc07.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        item = tc07.get_cards_items(getData["card"])
        self.execute_js("arguments[0].scrollIntoView();", item)
        actionChains.move_to_element_with_offset(item, 0, getData["offset"]).click().perform()

        classes = item.get_attribute("class")
        time.sleep(3)
        print(tc07.get_card_status().text)
        if "card-expired" in classes:
            comp = self.driver.find_element_by_css_selector(".saved-card-expired p").text
            assert comp == "Platnost karty vypršela"
            log.info("Status card-expired - OK")
            tc07.redirect_to_start()

        elif "card-unsupported" in classes:
            assert tc07.get_card_status().text == "Obchodník tuto kartu neakceptuje"
            log.info("Status card-unsupported - OK")
            tc07.redirect_to_start()

        else:
            self.explicit_wait_function_ID('cvc')
            assert tc07.fill_cvc().is_displayed()
            tc07.fill_cvc().send_keys(getData["cvc"])
            log.info("Status card - accepted - OK")
            tc07.confirm_payment_form().click()
            self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
            tc07.redirect_to_start()

        tc07.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_07_saved_cards)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param