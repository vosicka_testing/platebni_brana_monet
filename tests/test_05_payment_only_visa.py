#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_only_visa_05 import PaymentVisa05
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test05(BaseClass):

    def test_standard_payment(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc05 = PaymentVisa05(self.driver)
        # logger
        log = self.getLogger()
        tc05.select_test().click()
        tc05.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc05.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc05.fill_card_number().send_keys(getData["card_number"])
        tc05.click_show_popup_error().click()
        time.sleep(1)
        if tc05.check_popup_error() and getData["brand"] != "VISA":
            log.warning("Neakceptovaná platba brand: " + getData["brand"] + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"] + " - podle scenare OK")
            tc05.redirect_to_start()

        else:
            tc05.fill_expiration().send_keys(getData["expiration"])
            tc05.fill_cvc().send_keys(getData["cvc"])
            assert tc05.check_visa_icon()
            log.info("Akceptovaná platba brand: " + getData["brand"] + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"])
            tc05.confirm_payment_form().submit()
            self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
            tc05.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_05_payment_visa)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param
