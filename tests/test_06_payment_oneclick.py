#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_oneclick_06 import PaymentOneClick06
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test06(BaseClass):

    def test_standard_payment(self, getData):
        # Prvotní platba pro následné oneclick platby
        tc06 = PaymentOneClick06(self.driver)
        # logger
        log = self.getLogger()
        tc06.select_test().click()
        tc06.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc06.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        try:
            for pb in payment_buttons:
                assert pb.get_attribute("class") in payments
            assert len(payment_buttons) == 3
            not_found = False
        except:
            log.warning("Platby mpass@bank ani pt@bank nejsou dostupne - podle scenare OK")
            not_found = True
        assert not_found
        tc06.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc06.click_show_popup_error().click()
        tc06.fill_expiration().send_keys(getData["expiration"])
        tc06.fill_cvc().send_keys(getData["cvc"])
        try:
            tc06.save_card_check().click()
            not_found = False
        except:
            log.warning("Platba bez moznosti ulozit kartu  - podle scenare OK")
            not_found = True
        assert not_found
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc06.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        tc06.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_06_payment_oneclick)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param