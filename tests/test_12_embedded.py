#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_embedded_12 import PaymentEmbedded12
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test12(BaseClass):

    def test_standard_payment(self, getData):
        # Platba kartou v embedded bráně
        tc12 = PaymentEmbedded12(self.driver)
        # logger
        log = self.getLogger()
        tc12.select_test().click()
        tc12.confirm_test().click()
        # Prepnuti do frame modu
        self.switchToFrame()
        log.info("Brana v rezimu iframe")
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc12.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert not pb.get_attribute("class") in payments
        assert len(payment_buttons) == 0
        log.info("Platba bez platebniho tlacitka i MPASS - OK")
        tc12.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc12.fill_expiration().send_keys(getData["expiration"])
        tc12.fill_cvc().send_keys(getData["cvc"])
        try:
            tc12.save_card_check().click()
            not_found = False
            log.info("Platba s moznosti ulozit kartu - NOK")
        except:
            log.info("Platba bez moznosti ulozit kartu - podle scenare OK")
            not_found = True
        assert not_found
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc12.confirm_payment_form().submit()
        self.driver.switch_to_default_content()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        tc12.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_02_standard_payment)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param
