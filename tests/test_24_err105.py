#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err105_24 import PaymentErr24
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test24(BaseClass):

    def test_standard_payment(self, getData):
        # Platba pomocí pt@shop (ČSOB)
        tc24 = PaymentErr24(self.driver)
        # logger
        log = self.getLogger()
        tc24.select_test().click()
        tc24.confirm_test().click()
        try:
            assert tc24.error_block().is_displayed()
            log.info("Error message 105 displayed - OK")
            state = True
        except:
            log.warning("No error message - NOK")
            state = False
        assert state
        tc24.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param