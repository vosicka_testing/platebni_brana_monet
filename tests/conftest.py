#!/usr/bin/python
# -*- coding: utf-8 -*-

import pytest
from selenium import webdriver
import time
driver = None

def pytest_addoption(parser):
    parser.addoption(
        "--browser_name", action="store", default="chrome"
    )


@pytest.fixture(scope="class")
def setup(request):
    global driver
    value = request.config.getoption("--browser_name")
    if value == "firefox":
        driver = webdriver.Firefox(
            executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\geckodriver.exe')
    elif value == "chrome":
        driver = webdriver.Chrome(
            executable_path='/home/osboxes/Desktop/platebni_brana/continous_integration/drivers/chromedriver')
            #executable_path=r'C:\Users\vosicka\Documents\platebni_brana_final\selenium_brana\drivers\chromedriver.exe')
            #executable_path=r'C:\Users\osick\PycharmProjects\python_course\web_drivers\msedgedriver.exe')
    elif value == "edge":
        driver = webdriver.Edge(
            executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\msedgedriver.exe')
    elif value == "opera":
        driver = webdriver.Opera(
            executable_path=r'C:\Users\vosicka\Desktop\operadriver.exe')
    driver.implicitly_wait(5)
    driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")
    driver.maximize_window()
    request.cls.driver = driver
    yield
    time.sleep(3)
    driver.close()



@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
        Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
        :param item:
        """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when == 'call' or report.when == "setup":
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            file_name = report.nodeid.replace("::", "_") + ".png"
            _capture_screenshot(file_name)
            if file_name:
                html = '<div><img src="%s" alt="screenshot" style="width:304px;height:228px;" ' \
                       'onclick="window.open(this.src)" align="right"/></div>' % file_name
                extra.append(pytest_html.extras.html(html))
        report.extra = extra


def _capture_screenshot(name):
        driver.get_screenshot_as_file(name)
