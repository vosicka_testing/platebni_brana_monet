#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_no_buttons_08 import PaymentNoButtons08
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test08(BaseClass):

    def test_standard_payment(self, getData):
        # Standardní platba: pouze karta, bez mpass@bank, pt@bank
        tc08 = PaymentNoButtons08(self.driver)
        # logger
        log = self.getLogger()
        tc08.select_test().click()
        tc08.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc08.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        try:
            for pb in payment_buttons:
                assert pb.get_attribute("class") in payments
            assert len(payment_buttons) == 3
            not_found = False
            log.warning("Dostupne vice platebnich metod - NOK")
        except:
            log.warning("Platby mpass@bank ani pt@bank nejsou dostupne - podle scenare OK")
            not_found = True
        assert not_found
        tc08.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc08.click_show_popup_error().click()
        tc08.fill_expiration().send_keys(getData["expiration"])
        tc08.fill_cvc().send_keys(getData["cvc"])
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc08.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        tc08.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_08_no_buttons)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param