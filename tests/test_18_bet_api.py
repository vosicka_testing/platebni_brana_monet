#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_bet_api_18 import PaymentBetApi18
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test18(BaseClass):

    def test_standard_payment(self, getData):
        # Platba registrovanou kartou u sázkovky
        tc18 = PaymentBetApi18(self.driver)
        # logger
        log = self.getLogger()
        tc18.select_test().click()
        tc18.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc18.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 0
        log.info("Platba bez platebniho tlacitka i MPASS - OK")
        tc18.fill_cvc().send_keys(getData["cvc"])
        try:
            tc18.save_card_check().click()
            not_found = False
        except:
            log.warning("Platba bez moznosti ulozit kartu - podle scenare OK")
            not_found = True
        assert not_found
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc18.confirm_payment_form().click()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        tc18.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_02_standard_payment)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param