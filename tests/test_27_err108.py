#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err108_27 import PaymentErr27
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test27(BaseClass):

    def test_standard_payment(self, getData):
        # Statická stránka pro err108 (červená chyba)
        tc27 = PaymentErr27(self.driver)
        # logger
        log = self.getLogger()
        tc27.select_test().click()
        tc27.confirm_test().click()
        try:
            assert tc27.error_block().is_displayed()
            log.info("Message 108 displayed - Platba zrušena - OK")
            state = True

        except:
            log.warning("No message - NOK")
            state = False
        assert state
        tc27.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param