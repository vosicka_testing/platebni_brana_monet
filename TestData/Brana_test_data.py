#!/usr/bin/python
# -*- coding: utf-8 -*-

class HomePageData:

    test_01_standard_payment = [
        {"card_number":"4125010001000208","expiration":"1121", "cvc":"150","card_name":"VISA"},
        {"card_number":"5168440001000202","expiration":"0921", "cvc":"170","card_name":"MasterCard"}]

    test_02_standard_payment = [
        {"card_number": "4125010001000208", "expiration": "1121", "cvc": "150"},
        {"card_number": "5168440001000202", "expiration": "0921", "cvc": "170"}]

    test_03_only_cz_sk = [
        {"card_number": "4154610001000209", "expiration": "1121", "cvc": "150"},
        {"card_number": "4550550001000207", "expiration": "0921", "cvc": "170"}]

    test_04_only_csob = [
        {"card_number": "4407520211155310", "expiration": "1121", "cvc": "150"},
        {"card_number": "4154610001000209", "expiration": "0921", "cvc": "170"}]

    test_05_payment_visa = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard"},
        {"card_number": "4125010001000208", "expiration": "0921", "cvc": "170", "brand":"VISA"},
        {"card_number": "30569309025904", "expiration": "1123", "cvc": "180", "brand":"Diners"}]

    test_06_payment_oneclick = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard"},
        {"card_number": "4125010001000208", "expiration": "0921", "cvc": "170", "brand":"VISA"},
        {"card_number": "30569309025904", "expiration": "1123", "cvc": "180", "brand":"Diners"}]

    test_07_saved_cards = [{"card": 0, "offset": 0, "cvc": "173"}, {"card": 1, "offset": 30, "cvc": "173"},
                           {"card": 2, "offset": 40, "cvc": "173"},{"card": 3, "offset": 50, "cvc": "173"},
                           {"card": 4, "offset": 60, "cvc": "173"}, {"card": 5, "offset": 70, "cvc": "173"}]

    test_08_no_buttons = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard"},
        {"card_number": "4125010001000208", "expiration": "0921", "cvc": "170", "brand":"VISA"},
        {"card_number": "30569309025904", "expiration": "1123", "cvc": "180", "brand":"Diners"}]

    test_09_payment_no_pt = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard"},
        {"card_number": "4125010001000208", "expiration": "0921", "cvc": "170", "brand":"VISA"},
        {"card_number": "30569309025904", "expiration": "1123", "cvc": "180", "brand":"Diners"}]

    test_10_payment_no_mp = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard"},
        {"card_number": "4125010001000208", "expiration": "0921", "cvc": "170", "brand":"VISA"},
        {"card_number": "30569309025904", "expiration": "1123", "cvc": "180", "brand":"Diners"}]

    # pokud se jedna o type platebni tlacitko ERA/CSOB nutne zadat set_test - testy v ramci platebniho tlacitka
    test_11_platba_na_miru = [
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand": "MasterCard",
         "type": "omnibox"},
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand": "MasterCard",
         "type": "button-pay-csob", "set_test": 0},
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand": "MasterCard",
         "type": "button-pay-era", "set_test": 0},
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand": "MasterCard",
         "type": "button-pay-csob", "set_test": 1},
        {"card_number": "5168440001000202", "expiration": "1121", "cvc": "150", "brand":"MasterCard",
         "type":"button-pay-mpass"}]

    test_14_bet_api = [
        {"card_number": "000100", "expiration": "1121", "cvc": "150"},
        {"card_number": "000100", "expiration": "0921", "cvc": "170"}]

    # pokud se jedna o type platebni tlacitko ERA/CSOB nutne zadat set_test - testy v ramci platebniho tlacitka
    test_17_bet_api = [
        {"set_test": 0},
        {"set_test": 1},
        {"set_test": 2},
        {"set_test": 3},
        {"set_test": 4},
        {"set_test": 5},
        {"set_test": 6},
        {"set_test": 7}]