#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentSavedCards07:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.CLASS_NAME, "pay-by-card-button")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba dříve uloženou kartou")
    confirm_test_btn = (By.TAG_NAME, "input")
    cvc = (By.ID, "saved-card-cvc")
    expired_card = (By.CLASS_NAME, "replace-card-button")
    card_name = (By.ID, "cardname")

    card_items_set = (By.CSS_SELECTOR, ".card")
    card_status = (By.CLASS_NAME, "saved-card-status")

    def select_test(self):
        return self.driver.find_element(*PaymentSavedCards07.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentSavedCards07.confirm_test_btn)

    def fill_cvc(self):
        return self.driver.find_element(*PaymentSavedCards07.cvc)

    def save_card_name(self):
        return self.driver.find_element(*PaymentSavedCards07.card_name)

    def confirm_payment_form(self):
        return self.driver.find_element(*PaymentSavedCards07.payment_form)

    def redirect_to_start(self):
        return self.driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")

    # definice testu n. 7
    def get_cards_items(self,item):
        return self.driver.find_elements(*PaymentSavedCards07.card_items_set)[item]

    def get_card_status(self):
        return self.driver.find_element(*PaymentSavedCards07.card_status)