#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PlatebniTlacitko:

    def __init__(self, driver):
        self.driver = driver

    payment_link = (By.CSS_SELECTOR, "a")

    def process_buttons(self):
        return self.driver.find_elements(*PlatebniTlacitko.payment_link)

    def payment_pt(self, name):
        return self.driver.find_element_by_class_name(name)